# exceptions
'''try:
	fin = open("input.txt")
	for line in fin:
		print line
	fin.close() 
except ValueError:
	pass
except IOError:
	print "File not found."
	print "Executing file processing.."
finally:
	print "Entering finally for final processing."'''


#argument handling

'''import sys

print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument list:', str(sys.argv)
print 'The first argument after the script name is' , sys.argv[0]'''

#lambda operators

'''f = lambda x, y: x+y
print f(1,3)
g = lambda x, y: f(x,y)*5
print g(1,3)
h = lambda x, y:x(y,5)
print h(f, g(1,3))

a= lambda x,y: x+y
b = lambda x,y: x-y
c = lambda x,y: x*y
d = lambda x,y:x/y
e = lambda x,y,z:z(x,y)
x= 4
y=8
print e(x,y,a)
print e(x,y,b)
print e(x,y,c)'''

#map()function

a=[2,3,4,5]
f= lambda x:x**2
r = map(f,a)
print r


a = list()
for x in xrange (1,11):
	if x%2 == 0:
		a.append(x)
print a

print [x for x in xrange(1,11)]
print [x for x in xrange(1,11) if x%2==0]
f=lambda x:x*x
print [f(x) for x in xrange(1,11) if x%2!=0]
#print [[y for y in xrange(1, x+1)]for x in xrange(1,11)]
print [x for x in xrange(1,6) for y in xrange(1,9)]
print [[y for y in xrange(1, x+1)]for x in xrange(1,6)]
print [y for x in xrange(1,6) for y in xrange(1,x+1)]


#list comprehension
1,2,3,4,5
[x for x in xrange(1,6)]







