from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session

mod = Blueprint('items', __name__)

@mod.route('/')
def post_list():
    items = g.itemsdb.getItems(session['username'])
    return render_template('posts/post.html', items=items)

@mod.route('/', methods=['POST'])
def add_item():
    new_item = request.form['item'] 
    new_price= request.form['price'] 
    g.itemsdb.createItem(new_item, new_price, session['username']) 
    flash('New item added!', 'add_item_success')
    return redirect(url_for('.post_list'))

@mod.route('/carts', methods=['POST'])
def add_quantity():
    new_item = request.form['item'] 
    new_price= request.form['price'] 
    new_quantity =request.form['quantity']
    g.itemsdb.updateItem(new_item, new_price, new_quantity, session['username']) 
    flash('Added quantity!', 'add_quantity_success')
    return redirect(url_for('.post_list'))

@mod.route('/carts', methods=['POST'])
def add_to_cart():
    new_item = request.form['item'] 
    new_price= request.form['price'] 
    new_quantity =request.form['quantity']
    g.itemsdb.createItem(new_item, new_price, new_quantity, session['username']) 
    flash('Added to cart!', 'add_to_cart_success')
    return redirect(url_for('.add_cart'))