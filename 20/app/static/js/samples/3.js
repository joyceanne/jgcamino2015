var PostList = React.createClass({
    render: function() {
      return <p>Hello! I will become a PostList Soon!</p>
    }
});

var PostForm = React.createClass({
    render: function() {
      return <p>And I will become a form for posts!</p>
    }
});

var PostBox = React.createClass({
  render: function() {
    return (
      <div>
        <h1>Posts</h1>
        <PostList />
        <PostForm />
      </div>
    );
  }
});