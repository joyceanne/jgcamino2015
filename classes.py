
#classes whatever you put in classes technically become method


class vehicle(object): 			#class name(initialization):
	def __init__(self, name): 
		self.name = name   #constructor

	def change_gear(self):
		print "Manual Shift for " + self.name

vh = vehicle("Bugatti")
vh.change_gear() 

class boat(vehicle):			#inherits the initialization of vehicle
	'''def __init__(self,motor_type):  #makukuha ang __init__ ni boat, which is wala
		self.motor_type = motor_type'''

	def sail(self):
		print "Sailing on for " + self.name

vh = boat("My Ship")
vh.sail()
vh.change_gear()