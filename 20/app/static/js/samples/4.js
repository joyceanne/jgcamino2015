var Post = React.createClass({
  render: function() {
    var style = {
      backgroundColor: this.props.bgcolor
    }
    return (
      <div style={style}>
        {this.props.item_name}: {this.props.children}
      </div>
    );
  }
});

var PostList = React.createClass({
  render: function() {
    return (
      <div>
        <Post bgcolor="#FF6600" item_name="chocolate">Hello!</Post>
        <Post bgcolor="#99CCFF" item_name="pizza">And again, another greeting!</Post>
      </div>
    );
  }
});