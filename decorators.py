#decorators - a function that manipulates a function without changing the function itself
def logger(func):	#decorator function
	def inner(x,y):   #function closure
		if x==3:	#use if..else to manipulate the function 
			print "I don\'t want to multiply."
		else:
			print "Operands were: %d and %d and answer is %d." %(x,y, func(x,y))
		return func(x,y)
	def inner_two():	#second closure(optional)
		pass
	return inner


@logger
def multiply(x,y):
	return x*y

@logger
def divide(x,y):
	return x/y

def add(x,y):
	print x+y

multiply(3,4)
divide(100,5)
add(-2, 5)
