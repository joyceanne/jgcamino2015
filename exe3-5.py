a = list("door")
b = list("dog")

print [x for x in a if x in b]

a = "door"
b = "dog"
print [x for x in a]
#prints: ['d', 'o', 'o', 'r']
print [x for x in a if x in b]
#prints: ['d', 'o', 'o']

a = [x for x in a if x in b]
print a
c = ""
c = list()
[c.append(x) for x in a if x not in c]

#help("".join)
"".join

#answer 3.py, iprint lang yung magkaparehas, prints a in b
def extract_unique(a,b):
	c = []
	[c.append(x) for x in a if x in b and x not in c]
	print "".join(c)

extract_unique("door","dog")  #'do'
extract_unique("door","wood") #'do'
extract_unique("computer","glomp") #'omp'

#answer 4.py removes a in b
a = ['c','a','t']
b = "I can't spell catheter."
[x for x in b if x not in a]
print ''.join([x for x in b if x not in a])

#result: 'I n' spell heer.'

def remove_chars(a,b):
	return ''.join([x for x in b if x not in a])
print ''.join([x for x in b if x not in a])


#5.py
x = 3
y = 2
z = 6

[(a,b) in a in xrange(1,x+1) for b in xrange(1,y+1) if a*b == z]
l = [[a,b] in a in xrange(1,x+1) for b in xrange(1,y+1) if a*b == z]
set(l)
dir(tuple) #to check if may sort si tuple

l = [[a,b] in a in xrange(1,x+1) for b in xrange(1,y+1) if a*b == z]
f = lambda x:x.sort()
map(f,l)
#sorted na yung laman ng list, then convert it to tuple

m = [tuple(x) for x in l]


#answer
def extract_factors(x,y,z):
	l = [[a,b] in a in xrange(1,x+1) for b in xrange(1,y+1) if a*b == z]
	f = lambda x:x.sort()
	map(f,l)
	m = [tuple(x) for x in l]
	print set(m)


extract_factors(3,3,6)
extract_factors(5,10,20)













