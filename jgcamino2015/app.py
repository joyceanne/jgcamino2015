from flask import Flask
from flask import render_template
from flask import request

app = Flask(__name__)

@app.route('/')
def hello_world():
	return 'Hello World!'

@app.route('/<username>', methods=['GET', 'POST'])
def hello_username(username):
	if request.method == 'POST':
		for i in request.form:
			print i + ' ' + str(request.form[i])
		return render_template('hello_post.html',name=username)
	elif request.method == 'GET':
		for i in request.args:
			print i + ' ' + str(request.args[i])
		return render_template('hello.html',name=username)
	return 'Not supported'

@app.route('/<int:user_id>')
def hello_user_id(user_id):
	return 'Printing user id: %s.' %user_id

@app.route('/about')
def about_page():
	return 'This is the about page.'

if __name__ == '__main__':
	app.run()