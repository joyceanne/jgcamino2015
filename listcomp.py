#list comprehension
[x for x in xrange(1,11)]

print [x for x in xrange(1,11)
	if x%2 == 0]

print [x+5 for x in xrange(1,11)] #nadadagdagan ng 5 ang bawat number
print [x+5 for x in xrange(1,11) if (x+5)%6 == 0 or (x+5)%7 == 0]

print [x + sum([y for y in xrange(1, x)]) for x in xrange(1,7)]

print [x**2 for x in xrange(1,12) if x%2 == 1]