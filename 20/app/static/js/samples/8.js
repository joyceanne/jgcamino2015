var Post = React.createClass({
  render: function() {
    var style = {
      backgroundColor: this.props.bgcolor
    }
    return (
      <div style={style}>
        {this.props.item_name}: {this.props.children}
      </div>
    );
  }
});

var PostList = React.createClass({
  render: function() {
    var postNodes = this.props.data.map(function (comment, index) {
      return (
        <Post key={index} author={comment.item_name}>
          {comment.price}
        </Post>
      );
    });
    return (
      <div>
        {postNodes}
      </div>
    );
  }
});

var PostForm = React.createClass({
  handleSubmit: function(e) {
    e.preventDefault();
    var form = e.target;
    var item_name = form.item_name.value.trim();
    var price = form.price.value.trim();
    if (!price || !item_name) {
      return;
    }
    this.props.onCommentSubmit({item_name: item_name, price: price});
    form.item_name.value = '';
    form.price.value = '';
    return;
  },
  render: function() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input type="text" placeholder="item name" name="item_name"/>
        <input type="number" placeholder="price" name="price"/>
        <input type="submit" value="Post" />
      </form>
    );
  }
});

var PostBox = React.createClass({
  loadCommentsFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        var postDict = JSON.parse(JSON.stringify(data));   
        this.setState({data: postDict['posts']});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  handleCommentSubmit: function(comment) {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      type: 'POST',
      data: comment,
      success: function(data) {
        var postDict = JSON.parse(JSON.stringify(data));   
        this.setState({data: postDict['posts']});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {data: []};
  },
  componentDidMount: function() {
    this.loadCommentsFromServer();
    setInterval(this.loadCommentsFromServer, this.props.pollInterval);
  },
  render: function() {
    return (
      <div>
        <h1>Inventory</h1>
        <PostList data={this.state.data} />
        <PostForm onCommentSubmit={this.handleCommentSubmit}/>
      </div>
    );
  }
});