#Generators

#yield - pauses the execution of a function

import random

def lottery():
	for i in xrange(6):
		yield random.randint(1, 40)
	yield random.randint(1,15)

for random_number in lottery():
	print "And the next number is...", random_number


def sim_xrange(n):
	i = 0
	while i < n:
		yield i
		i += 1
for x in sim_xrange(10):
	print x


a = 'Hello'
print [x for x in a] #list comprehension
g = (x for x in a) #generator comprehension
for x in g:
	print x


'''a = 3
g = (x for x in xrange(a-1, 3))
for x in g:
	print sum(x)'''



def fibo(n):
	i = n-2
	while i < n:
		yield i
		i += 1
for x in fibo(10):
	print x

