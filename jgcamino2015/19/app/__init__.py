from flask import Flask
from flask import g

from modules import default
from modules import items
from modules import util

from pymongo import MongoClient

app = Flask(__name__)

app.session_interface = util.MongoSessionInterface(db='sessions')

def get_main_db():
    client = MongoClient('mongodb://localhost:27017/')
    maindb = client.itemsdb
    return maindb

@app.before_request
def before_request():
    mainDb = get_main_db()
    g.usersdb = items.UserDB(conn=mainDb.users)
    g.itemsdb = items.ItemDB(conn=mainDb.items)     
    g.cartsdb = items.CartDB(conn=mainDb.carts)    

app.register_blueprint(default.mod)
app.register_blueprint(items.mod, url_prefix="/items")
#app.register_blueprint(carts.mod, url_prefix="/carts")

