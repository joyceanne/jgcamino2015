def fibo(n):
	a = 1
	b = 0
	for x in xrange(n):
		yield a
		c = b
		b = a
		a = a + c

for x in fibo(5):
	print x