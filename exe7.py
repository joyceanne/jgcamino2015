'''
a = [1,2,3,4,5]
a = [str(x) for x in a] #to convert a list to string
print a
''.join(a)
b = ''.join(a)
b = list(b) #to convert a string to list
'''

def swap(a,b):
	count = 0
	for x in b:  #to count how many elements are in b
		count += 1
	a = list(a)
	b = list(b)
	for x in xrange(count/2):
		if b[x] in a:
			temp = b[x]
			b[x] = b[(x + 1)*-1]
			b[(x+1)*-1] = temp
	return ''.join(b)

print swap("cat","catheter")
print swap("damn","badminton")