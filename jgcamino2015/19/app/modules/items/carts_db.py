class CartDB:
    def __init__(self, conn):
        self.conn = conn

    def getCarts(self, username):
        return self.conn.find({'username': username})

    def createCart(self, item, price, quantity, username):
        self.conn.insert({'item':item, 'price':price, 'quantity':quantity, 'username': username}) 

    