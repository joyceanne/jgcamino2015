from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session

mod = Blueprint('carts', __name__)

@mod.route('/')
def cart_list():
    carts = g.cartsdb.getCarts(session['username'])
    return render_template('posts/post.html', carts=carts)

@mod.route('/', methods=['POST'])
def add_cart():
    new_item = request.form['item'] 
    new_price= request.form['price']
    new_quantity= request.form['quantity']  
    g.cartsdb.createCart(new_item, new_price, new_quantity, session['username']) 
    flash('Added to cart!', 'add_to_cart_success')
    return redirect(url_for('.cart_list'))
