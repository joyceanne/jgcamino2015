
#module - magkapareho ng folder
#package - magkaiba ng folder
#partial functions

#partial exponents
from functools import partial

def exponent(base, exp):
	return base**exp

squares = partial(exponent, 2)
print squares(4)

cubes = partial(exponent, 3)
print cubes(3)

#base
from functools import partial

def exponent(exp, base):
	return base**exp

base = partial(exponent, 2)
print base(4)


#arguments
def sim_sum(*args):
	sum_val = 0
	for x in args:
		sum_val += x
	return sum_val

print sim_sum(2,3,4)
print sim_sum(2,3,4,5)



#keywords
def sim_sum(**kwargs):
	sum_val = 0
	for x in kwargs['number']:
		sum_val += x
	return sum_val

print sim_sum(number = [1,2,3,4,5])


def sum_of_three(first, second, third):
	return first + second + third

add_to_three = partial(sum_of_three, 5)
print add_to_three(4,6)


def sum_of_three(first, second, *args):
	return first + second + sum(args)

add_to_three = partial(sum_of_three, 3,5,4)
print add_to_three(4,6)




