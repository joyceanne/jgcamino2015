class ItemDB:
    def __init__(self, conn):
        self.conn = conn

    def getItems(self, username):
        return self.conn.find({'username': username})

    def createItem(self, item, price, username):
        self.conn.insert({'item':item, 'price':price, 'username': username}) 

    def updateItem(self, item, price, quantity, username):    
        self.conn.update({'item':item, 'price':price, 'quantity':quantity, 'username': username}) 